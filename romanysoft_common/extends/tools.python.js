/**
 * Created by Ian on 2015/8/15.
 * 1.创建声明：（1）Python服务器的回调处理简单话，进入该js文件，自动初始化
 *            （2）创建本地WebSocket服务，只需要使用startPyWebServer
 *            （3）系统默认自动创建与WebSocket服务器的自动连接
 */

(function () {
    window['UI'] = window['UI'] || {};
    window.UI.c$ = window.UI.c$ || {};
})();


(function () {
    var c$ = {};
    c$ = $.extend(window.UI.c$, {});
    var b$ = window.BS.b$;

    var pyServerPrefix = 'PyWebServer';      // Python服务器任务的前缀
    c$.pythonMapPlugins = {
        PythonCLIPlugin:{
            callMethod:"task",
            type:"calltask",
            tool:{
                appPath:b$.getAppPluginDir() + "/pythonCLI.app/Contents/MacOS/pythonCLI",
                command:[],
                mainThread:false
            }
        }
    };
    c$.pyWS = null;
    c$.pyWS_ID = null;
    c$.python = {
        
        debugLog:true,  //默认打印debug信息

        isPyWSisRunning:false, //Python服务器是否正在运行

        // 配置是否打印多余的Debug信息
        configDebugLog:function(enable){
            this.debugLog = enable;
        },

        // 注册PythonWS的回调句柄
        registerPyWSMessageCB: function (cb) {
            c$.pyWS_cb = cb;
        },

        // 启动Python Web服务
        startPyWebServer: function (e) {
            var taskID = "undefined";
            if (b$.pNative) {
                var copyPlugin = $.objClone(c$.pythonMapPlugins.PythonCLIPlugin); // 复制一个插件副本

                var workDir = b$.App.getAppResourceDir() + "/data/python";
                var resourceDir = b$.App.getAppDataHomeDir() + "/Resources";
                var configFile = "Resources/config.plist";
                var pythonCommand = " --port=" + b$.App.getServerPort();

                var regCommand = '["-i","pythonCLI","-r","%resourceDir%","-m","%command%"]';

                var formatCommonStr = regCommand;
                formatCommonStr = formatCommonStr.replace(/%resourceDir%/g, resourceDir);
                formatCommonStr = formatCommonStr.replace(/%command%/g, pythonCommand);

                var command = eval(formatCommonStr); // 转换成command
                copyPlugin.tool.command = command;

                taskID = pyServerPrefix + (new Date()).getTime();
                b$.createTask(copyPlugin.callMethod, taskID, [copyPlugin.tool]);
            }

            return taskID;
        },

        // 自动处理连接问题
        autoCreatePyWS: function (msec) {
            var t$ = this;

            var id = 'pyTime' + (new Date()).getTime() + parseInt(Math.random()*100000+1);
            window[id] = setInterval(function () {
                if (typeof c$.pyWS === "undefined" || c$.pyWS == null || c$.pyWS.readyState == 3) {
                    //尝试新的连接
                    console.log('[PyWS] 重新连接 localhost socket server... port==' + b$.App.getServerPort());
                    t$.createPyWS();
                } else {
                    clearInterval(window[id]);
                }
            }, msec || 2000);
        },


        // 建立Py Web socket 客户端
        createPyWS: function () {
            var t$ = this;

            var url = "ws://localhost:" + b$.App.getServerPort() + "/websocket";
            var WebSocket = window.WebSocket || window.MozWebSocket;

            console.log("wsurl = " + url);
            try {
                c$.pyWS = new WebSocket(url);    //启动监听服务 'ws://localhost:8124/';
                if (c$.pyWS) {
                    c$.pyWS.onopen = function (evt) {
                        // 注册自己的ID
                        c$.pyWS_ID = 'ws' + (new Date()).getTime() + parseInt(Math.random()*100000+1);

                        console.log("[PyWS] 已经连接上...");
                        c$.pyWS.send(JSON.stringify({'user_id': c$.pyWS_ID, 'msg_type': 'c_notice_id_Info'}));
                        t$.isPyWSisRunning = true;
                    };

                    c$.pyWS.onmessage = function (evt) {
                        if (typeof c$.pyWS_cb === 'undefined') {
                            alert(evt.data);
                        }
                        if(t$.debugLog == true){console.log(evt.data);}
                      
                        c$.pyWS_cb && c$.pyWS_cb(evt.data);
                    };

                    c$.pyWS.onerror = function (evt) {
                        console.log(evt);
                    };

                    c$.pyWS.onclose = function (evt) {
                        console.log('pyWS onclose: code == ', evt);
                        t$.isPyWSisRunning = false;
                        t$.autoCreatePyWS();
                    };
                }
            } catch (e) {
                console.log('pyWS 创建失败...');
                t$.isPyWSisRunning = false;
                console.warn(e)
            }


        }
    };

    c$.pythonAddon = {
        _pcb_idx: 0, // 回调函数
        getNewCallbackName: function (func, noDelete) {
            window._pythonCallback = window._pythonCallback || {};
            var _pcb = window._pythonCallback;
            var r = 'pcb' + ++c$.pythonAddon._pcb_idx;
            _pcb[r] = function () {
                try {
                    if (!noDelete) delete _pcb[r];
                } catch (e) {
                    console.error(e)
                }
                func && func.apply(null, arguments);
            };

            return '_pythonCallback.' + r;
        },


        current_task_idx: 0, // 默认任务ID
        // 私有函数
        common_service: function (cli, command, cb) {
            var currentTaskID = 'PYTASKID_' + ++c$.pythonAddon.current_task_idx;
            var callbackFunName = c$.pythonAddon.getNewCallbackName(function (obj) {
                        cb && cb(obj);
                    }, true);
            var obj = {
                'taskInfo': {
                    'task_id': currentTaskID,
                    'callback': callbackFunName,
                    'cli': cli || '',
                    'command': command || []
                },
                'msg_type': 'c_task_exec',
                'user_id': c$.pyWS_ID
            };

            c$.pyWS.send(JSON.stringify(obj));
            return {currentTaskID:currentTaskID, callBackFunName:callbackFunName};
        }

        , unknown: ''
    };

    //---------------------------------------------------------------------------------
    //绑定Python与Javascript的消息处理方式
    c$.jCallbackPython = $.Callbacks();
    c$.process_jCallbackPython = function(obj){
        var fireType = obj.type;

        // Python的处理部分，将转移到各自的回调函数中
        if(fireType.indexOf('python_',0) > -1){
            console.log('##RecivePythonMessage:\n' + $.obj2string(obj));
            try{
                var callback = obj.data['cb'];
                if (typeof eval(callback) == 'function' && callback.length > 0 ){
                    var jsString = callback + '(obj)';
                    try{
                        eval(jsString)
                    }catch(e){console.error(e)}
                }
            }catch(e){console.error(e)}
        }
    };
    c$.jCallbackPython.add(c$.process_jCallbackPython);

    //---------------------------------------------------------------------------------
    c$.python.registerPyWSMessageCB(function(data){
        var jsonObj = JSON.parse(data);
        var msgType = jsonObj['msg_type'];

        //
        if(msgType == 's_get_id_Info'){
            console.info('server getting the client id');
            c$.jCallbackPython.fire({type:'python_server_get_id_info', data:{}});
        }

        // 核心处理部分
        if(msgType == 's_task_exec_running') {
            c$.jCallbackPython.fire({type: 'python_task_running', data: jsonObj})
        }else if(msgType == 's_task_exec_feedback'){
            c$.jCallbackPython.fire({type:'python_task_feedback',data:jsonObj})
        }else if(msgType == 's_task_exec_result'){
            c$.jCallbackPython.fire({type:'python_task_finished',data:jsonObj})
        }else if(msgType == 's_err_progress'){
            c$.jCallbackPython.fire({type:'python_task_error', data:jsonObj, error:jsonObj.content})
        }
    });

    // 启动自动连接
    c$.python.autoCreatePyWS(1000);


    window.UI.c$ = $.extend(window.UI.c$, c$);
})();