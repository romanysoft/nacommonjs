nACommonJS 部分说明：

##根据工程，按需要使用，升级js的都是兼容之前的版本的，所以无需担心！ 
##在工程中，用到哪个就用哪个，不用全部使用所有的js及css




2015年5月15日 20:33:32

Mui 升级到0.1.2


2015年5月14日10:43:14
大部分内容更新

包括加入插件、字体部分

（1）Mui 升级到0.1.0
（2）jquery 下载1.11.3 及2.1.4版本
 (3) 变更jquery的插件位置



2015年1月12日20:20:01
bs.js 加入
BS.b$.App.getCompatibleWebkitLanguageList('webkitCompatible')  获取浏览器兼容的语言标识列表 //["zh", "zh-CN", "zh-cn", "zh-Hans"]
BS.b$.App.getCompatibleWebkitLanguageList() 通过系统本地，来获取浏览器兼容的语言标识

参照鹏飞的做法：添加b$._get_callback 来获取临时函数


2014年12月29日20:05:31
angular-translate 是一个 AngularJS 的模块，用于简化 i18n 和 l10n 应用的便携，实现了延迟加载和多元化。


============================================================================================================================================






2014年12月22日10:54:52
angular-material 更新



============================================================================================================================================
2014年12月10日21:54:29
针对bs.js 添加导入文件及保存文件的回调返回的数据样例

    // 导入文件
    /**
     BS.b$.cb_importFiles({
         "success":true,
         "parentDir":"/Volumes/DiskShareUser/Users/ian/TestResource/xls",
         "filesCount":1,
         "filesArray":[
            {"isExecutable":true,
            "isDeletable":false,
            "fileNameWithoutExtension":"Book1",
            "fileName":"Book1.xls",
            "fileSize":7680,
            "fileSizeStr":"7.7KB",
            "fileUrl":"file:///Volumes/DiskShareUser/Users/ian/TestResource/xls/Book1.xls",
            "isReadable":true,
            "isWritable":true,
            "extension":"xls",
            "filePath":"/Volumes/DiskShareUser/Users/ian/TestResource/xls/Book1.xls"
            }
         ]
     });
    **/

    // 选择输出文件
    /*
     BS.b$.cb_selectOutFile({
         "success":true,
         "fileName":"untitled.csv",
         "fileUrl":"file:///Volumes/DiskShareUser/Users/ian/TestResource/xls/untitled.csv",
         "fileNameWithoutExtension":"untitled",
         "extension":"csv",
         "filePath":"/Volumes/DiskShareUser/Users/ian/TestResource/xls/untitled.csv"
     });
     */

============================================================================================================================================

2014年12月9日23:39:05
针对BS.js 添加如下帮助函数

        ///路径是否存在
        checkPathIsExist:function(path){
            if(b$.pNative){
                return b$.pNative.path.pathIsExist(path);
            }

            return false;
        },

        ///文件是否为0Byte
        checkFileIsZero:function(file_path){
            if(b$.pNative){
                return b$.pNative.path.fileIsZeroSize(file_path);
            }

            return false;
        },

        ///路径是否可以写
        checkPathIsWritable:function(path){
            if(b$.pNative){
                return b$.pNative.path.checkPathIsWritable(path);
            }

            return false;
        },

        ///判断路径是否可读
        checkPathIsReadable:function(path){
            if(b$.pNative){
                return b$.pNative.path.checkPathIsReadable(path);
            }

            return false;
        },

        ///判断路径是否可运行
        checkPathIsExecutable:function(path){
            if(b$.pNative){
                return b$.pNative.path.checkPathIsExecutable(path);
            }

            return false;
        },

        ///判断路径是否可删除
        checkPathIsDeletable:function(path){
            if(b$.pNative){
                return b$.pNative.path.checkPathIsDeletable(path);
            }

            return false;
        },


        ///判断是否为文件
        checkPathIsFile:function(path){
            if(b$.pNative){
                return b$.pNative.path.checkPathIsFile(path);
            }

            return false;
        },

        ///判断是否为目录
        checkPathIsDir:function(path){
            if(b$.pNative){
                return b$.pNative.path.checkPathIsDir(path);
            }

            return false;
        },

        ///获取文件扩展名
        getFileExt:function(path){
            if(b$.pNative){
                return b$.pNative.path.getFileExt(path);
            }

            return "";
        },

        ///获取路径上一级目录路径
        getPathParentPath:function(path){
            if(b$.pNative){
                return b$.pNative.path.getPathParentPath(path);
            }

            return "";
        },


        ///获取文件的基本属性
        getFilePropertyJSONString:function(path){
            if(b$.pNative){
                return b$.pNative.path.getFilePropertyJSONString(path);
            }

            return "";
        },


        ///获得文件/目录size(实际字节数 1024)
        fileSizeAtPath:function(path){
            if(b$.pNative){
                return b$.pNative.app.fileSizeAtPath(path);
            }

            return "";
        },

        ///获得文件/目录占用磁盘(字节数 1000)
        diskSizeAtPath:function(path){
            if(b$.pNative){
                return b$.pNative.app.diskSizeAtPath(path);
            }

            return "";
        },

        ///获得字符串的md5值
        md5Digest:function(str){
            if(b$.pNative){
                return b$.pNative.app.md5Digest(str);
            }

            return "";
        }



===================================================================================================================================

2014年12月9日16:43:31
更新到angular-material 到最新版本


===================================================================================================================================
2014年12月9日12:35:56
1. 按照https://code.angularjs.org/1.3.6/ 更新angular库


===================================================================================================================================

2014年12月9日11:19:18
1.JS添加及升级
 (1) angular.min.js 升级到1.3.6
 (2) 引入angular-resource.min.js 1.3.6
 (3) 引入angular-route.min.js 1.3.6
 (4) 引入angular-aria.min.js 1.3.6
 (5) 引入angular-material
 (6) 引入hammerjs
 
2. 【Google Material Design】引入https://material.angularjs.org/#/demo/material.components.button 样式处理
	参照：https://github.com/angular/bower-material

3. Common目录内部结构，按照组件方式整理




===================================================================================================================================

2014年12月8日21:18:53
1.bs.js 添加获取服务器开放的端口
        ///{获取开通的服务器端口}
        getServerPort:function(){
            if(b$.pNative){
                return b$.pNative.app.getHttpServerPort();
            }

            return 8888; // 默认返回8888
        }

用法：BS.b$.App.getServerPort()

2.bs.js 规范b$.enablePluginCore 启动核心插件的功能
  过滤掉plugin["callMethod"] 不为 'call' 的插件映射

===================================================================================================================================
2014年11月21日10:37:47
1. bs.js 加入NSuserDefaults的处理功能。
   主要可以通过setOptions_RateAppClose 来有效屏蔽评价功能。该功能，主要是App运行不正常，可以手动屏蔽评价功能开启
   
   
2. util.js 新增 $.reportInfo 函数
   主要是为了收集App的相关信息，包括错误信息。 附加了语言及地理信息
   
   用途实例：汇报错误
    c$.reportError = function(error){
        var appName = BS.b$.App.getAppName();
        var appVersion = BS.b$.App.getAppVersion();
        var sn = BS.b$.App.getSerialNumber();
        var info = BS.b$.App.getRegInfoJSONString();
        var sandbox = BS.b$.App.getSandboxEnable();
        $.reportInfo({
            appName:appName,
            version:appVersion,
            sn:sn,
            info:info,
            sandbox:sandbox
        });
    };
   